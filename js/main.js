// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/9.12.1/firebase-app.js";

import { getDatabase, onValue, ref, get, set, child, update, remove }
    from "https://www.gstatic.com/firebasejs/9.12.1/firebase-database.js";

import { getStorage, ref as refS, uploadBytes, getDownloadURL }
    from "https://www.gstatic.com/firebasejs/9.12.1/firebase-storage.js";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyCI9Wl8yta1lWKCRWhz6KFm_3na3MVG1mU",
    authDomain: "proyecto-2020030212.firebaseapp.com",
    databaseURL: "https://proyecto-2020030212-default-rtdb.firebaseio.com",
    projectId: "proyecto-2020030212",
    storageBucket: "proyecto-2020030212.appspot.com",
    messagingSenderId: "770755508414",
    appId: "1:770755508414:web:3ef6e8ca34e11a125bc6ef",
    measurementId: "G-NER7DVWY7D"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const db = getDatabase();


var btnIniciar = document.getElementById('iniciar');
var btnRegistro = document.getElementById('registrar');

var login = document.getElementById('login');
var formulario = document.getElementById('formulario');
var Contraseña;
var usuario = document.getElementById("usuario").value;
var contraseña = document.getElementById("contraseña").value;
var verificador = document.getElementById("verificador").value;
var Token;
function inicioSesion() {


    usuario = document.getElementById("usuario").value;
    contraseña = document.getElementById("contraseña").value;
    if (usuario == "ADMIN" && contraseña == "ADMIN") {
        alert("BIENVENIDO ADMINISTRADOR.");
        window.location.href = "vistaadmin.html";
    }
    else {
        alert("USUARIO O CONTRASEÑA ERRONEA");
    }
}


function buscarDatos() {

    leerInputs();
    const dbref = ref(db);
    get(child(dbref, 'usuarios/' + usuario)).then((snapshot) => {
        if (snapshot.exists()) {

            Contraseña = snapshot.val().Contraseña;
            Token = snapshot.val().Token;
            comprobar();

        }
        else {
            alert("Usuario O Contraseña Incorrectos");
        }
    }).catch((error) => {
        alert("ERROR" + " " + error);
    });

}

function verificar() {
    leerInputs();
    if (usuario == "" || contraseña == "") {
        alert("INSERTE ALGUN VALOR");
    } else {
        buscarDatos();
    }
}
function comprobar() {
    if (contraseña == Contraseña) {
        alert("Bienvenido " + usuario)
        llenarInput();
        invisible();
    }
    else {
        alert("Usuario O Contraseña Incorrectos")
    }
}
function invisible() {
    login.style.display = "none";
    formulario.style.display = "block";
    

}
function llenarInput() {
    document.getElementById('verificador').value = Token;
}
function leerInputs() {
    usuario = document.getElementById("usuario").value;
    contraseña = document.getElementById("contraseña").value;
}
btnIniciar.addEventListener('click', verificar);
